﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QuerySupplier.aspx.cs" Inherits="SP_QuerySupplier" %>

<%@ Register Src="~/Header.ascx" TagName="Header" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Css/Style.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        span.labelcp
        {
            float: left;
            display: block;
            width: 60px;
        }
        span.label
        {
            float: left;
            display: block;
            width: 80px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:Header ID="header" runat="server" Judul="Supplier Center" />
    <div id="content">
        <ul class="trail" id="link" runat="server">
            <li><a href="Default.aspx">Supplier Center</a></li>
            <li>></li>
            <li>Data Supplier</li>
        </ul>
        <br class="clear" />
        <br />
        <b id="baru" runat="server" />
        <div id="sbox" runat="server">
            <input type="text" class="bug" />
            <p>
                <asp:DropDownList ID="tipe" runat="server" Width="200">
                    <asp:ListItem>Tipe :</asp:ListItem>
                    <asp:ListItem Value="0">Perorangan</asp:ListItem>
                    <asp:ListItem Value="1">Badan Hukum Non-PKP</asp:ListItem>
                    <asp:ListItem Value="2">Badan Hukum PKP</asp:ListItem>
                    <asp:ListItem Value="3">Grup Internal</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="inaktif" runat="server" Width="150">
                    <asp:ListItem Value="0" Selected="True">Status Aktif</asp:ListItem>
                    <asp:ListItem Value="1">Status Inaktif</asp:ListItem>
                </asp:DropDownList>
            </p>
            <p>
                <asp:DropDownList ID="field" runat="server">
                    <asp:ListItem>Search :</asp:ListItem>
                    <asp:ListItem Value="SupplierID">Kode Supplier</asp:ListItem>
                    <asp:ListItem Value="Nama">Nama Lengkap</asp:ListItem>
                    <asp:ListItem Value="Bisnis1">Nama Bisnis #1</asp:ListItem>
                    <asp:ListItem Value="Bisnis2">Nama Bisnis #2</asp:ListItem>
                    <asp:ListItem Value="Bisnis3">Nama Bisnis #3</asp:ListItem>
                    <asp:ListItem Value="CP1">CP Finance</asp:ListItem>
                    <asp:ListItem Value="CP2">CP Marketing</asp:ListItem>
                    <asp:ListItem Value="CP3">CP Support</asp:ListItem>
                    <asp:ListItem Value="CP4">CP Manager</asp:ListItem>
                    <asp:ListItem Value="Rekening">Rekening Bank</asp:ListItem>
                    <asp:ListItem Value="Identitas">No. Identitas</asp:ListItem>
                    <asp:ListItem Value="NPWP">No. NPWP</asp:ListItem>
                    <asp:ListItem Value="NoHP">No. HP</asp:ListItem>
                    <asp:ListItem Value="NoTelp">No. Telepon</asp:ListItem>
                    <asp:ListItem Value="NoFax">No. Fax</asp:ListItem>
                    <asp:ListItem Value="Email">Alamat Email</asp:ListItem>
                    <asp:ListItem Value="Alamat">Alamat Domisili</asp:ListItem>
                    <asp:ListItem Value="AlamatTagih">Alamat Penagihan</asp:ListItem>
                    <asp:ListItem Value="AlamatSurat">Alamat Surat Menyurat</asp:ListItem>
                    <asp:ListItem Value="AlamatIdentitas">Alamat Identitas</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="keyword" runat="server" />
                <asp:Button ID="search" runat="server" OnClick="search_Click" Text="Display" />
                <asp:Button ID="excel" runat="server" OnClick="excel_Click" Text="Excel" Width="75" />
                <asp:Button ID="prt" runat="server" OnClick="prt_Click" Text="Print" Width="75" />
            </p>
        </div>
        <asp:Table ID="tb" runat="server">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell ColumnSpan="13" CssClass="center">
                       Data Supplier<br /><br />
                </asp:TableHeaderCell>
            </asp:TableHeaderRow>
            <asp:TableHeaderRow>
            <asp:TableHeaderCell>No</asp:TableHeaderCell>
                <asp:TableHeaderCell HorizontalAlign="Left">Kode Supplier</asp:TableHeaderCell>
                <asp:TableHeaderCell>Nama Lengkap</asp:TableHeaderCell>
                <asp:TableHeaderCell>Nama Bisnis</asp:TableHeaderCell>
                <asp:TableHeaderCell>No. NPWP</asp:TableHeaderCell>
                <asp:TableHeaderCell>No. HP</asp:TableHeaderCell>
                <asp:TableHeaderCell>No. Telp</asp:TableHeaderCell>
                <asp:TableHeaderCell>No. Fax</asp:TableHeaderCell>
                <asp:TableHeaderCell>Email</asp:TableHeaderCell>
                <asp:TableHeaderCell>Alamat Domisili</asp:TableHeaderCell>
                <asp:TableHeaderCell>Alamat Penagihan</asp:TableHeaderCell>
                <asp:TableHeaderCell>Alamat Surat Menyurat</asp:TableHeaderCell>
                <asp:TableHeaderCell>Alamat Identitas</asp:TableHeaderCell>
            </asp:TableHeaderRow>
        </asp:Table>
    </div>
    </form>
</body>
</html>
