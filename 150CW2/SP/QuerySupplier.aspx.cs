﻿using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Drawing;
using System.Linq.Dynamic;

public partial class SP_QuerySupplier : System.Web.UI.Page
{
    DbDataContext db = new DbDataContext();
    protected string AppModul { get { return "Supplier Center"; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        App.SetPage(this);
        App.SetModul(this, AppModul);
        Auth.SafeGuard(this, "SpSupplier");
        keyword.Focus();

        if (!IsPostBack)
        {
            fill();
        }
    }

    protected void search_Click(object sender, EventArgs e)
    {
        fill();
    }

    protected void excel_Click(object sender, EventArgs e)
    {
        fill();
        Lap.ExportExcel(this, "Data Supplier", tb);

    }

    protected void fill()
    {
        string w = "";
        if (tipe.SelectedIndex > 0) w += " AND Tipe = @1";
        if (inaktif.SelectedIndex == 1) w += " AND Inaktif = False";
        if (inaktif.SelectedIndex == 2) w += " AND Inaktif = True";

        byte Tipe = 0;
        if (tipe.SelectedIndex > 0) Tipe = Convert.ToByte(tipe.SelectedValue);

        var rs = from p in db.SpSuppliers.Where(App.SearchSql(field) + w, keyword.Text, Tipe)
                 orderby p.Inaktif, p.Nama
                 select p;
        int i = 1;
        foreach (var r in rs)
        {
            TableRow tr = new TableRow();
            TableCell c;

            tb.Controls.Add(tr);
            c = new TableCell();
            c.Text = i.ToString();
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = "<a href=\"SupplierFile.aspx?id=" + r.SupplierID + "\" target=\"_blank\">" + r.SupplierID + "</a>";
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.Nama;
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.Bisnis1;
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.NPWP;
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.NoHP;
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.NoTelp;
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.NoFax;
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.Email;
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.Alamat;
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.AlamatTagih;
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.AlamatSurat;
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = r.AlamatIdentitas;
            tr.Cells.Add(c);

            i++;
        }


    }

    protected void prt_Click(object sender, EventArgs e)
    {
        sbox.Visible = false;
        link.Visible = false;
        header.Visible = false;
        fill();
        Js.AutoPrint(this);
    }

}
