﻿using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Drawing;
using System.Linq.Dynamic;

public partial class TN_SummaryTenant : System.Web.UI.Page
{
    DbDataContext db = new DbDataContext();
    protected string AppModul { get { return "Tenancy"; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        App.SetPage(this);
        App.SetModul(this, AppModul);
        Auth.SafeGuard(this, "Tn");

        if (!Page.IsPostBack)
        {

            Response.Clear();
            filter();
            fill(false);
        }
    }
    protected void search_Click(object sender, EventArgs e)
    {
        if (valid)
            fill(false);
    }
    protected void excel_Click(object sender, EventArgs e)
    {
        if (valid)
        {

            Response.Clear();
            fill(true);
            Lap.ExportExcel(this, "Laporan Summary Tenant", tb);
        }
    }


    protected void filter()
    {
        LibTN.ListLokasiTenant(lokasi);
        LibGL.ListDept(dept, LibGL.ListAksesDept(Auth.UserID));
        LibTN.ListStrata(strata);
        dari.Text = Cf.Tgl(Cf.AwalBulan());
        sampai.Text = Cf.Tgl(Cf.AkhirBulan());
    }
    protected bool valid
    {
        get
        {
            bool x = true;
            x = Fv.isTgl(dari.Text, sampai.Text);

            if (!x) Js.AlertInvalid(this);
            return x;
        }
    }
    protected void fill(bool Excel)
    {
        DateTime Dari = Convert.ToDateTime(dari.Text);
        DateTime Sampai = Convert.ToDateTime(sampai.Text);

        //Cf.SwapTgl(dari.Text, sampai.Text, ref Dari, ref Sampai);

        string w = " AND 1=1";

        if (lokasi.SelectedIndex > 0) w += " AND Lokasi = @1";
        if (dept.SelectedIndex > 0) w += " AND Dept = @2";
        if (strata.SelectedIndex == 2)
            w += " AND StrataTitle = true";
        else if (strata.SelectedIndex == 1)
            w += " AND StrataTitle = false";
        else
            w += "";




        var rs = from p in db.TnTenants.Where(App.SearchSql(field) + w, keyword.Text, lokasi.SelectedValue, dept.SelectedValue,strata.SelectedValue)
                 where (
                        (p.TglMulai >= Dari && p.TglSelesai <= Sampai) ||
                        (p.TglMulai < Dari && p.TglSelesai > Sampai) ||
                        (p.TglSelesai >= Dari && p.TglSelesai <= Sampai) ||
                        (p.TglMulai >= Dari && p.TglMulai <= Sampai)
                    )
                 orderby p.TenantID
                 select new
                 {
                     p.TenantID,
                     p.TglMulai,
                     p.TglSelesai,
                     p.CustomerID,
                     p.Nama,
                     p.NamaBisnis,
                     p.Unit,
                     p.Lokasi,
                     p.Luas
                 };

        foreach (var r in rs)
        {
            if (!Response.IsClientConnected) break;

            TableRow tr = new TableRow();
            tb.Rows.Add(tr);
            TableCell c;

            tr.CssClass = "";
            //Kode Tenant
            c = new TableCell();
            c.Text = Excel ? r.TenantID : "<a href=\"TenantFile.aspx?id=" + r.TenantID + "\" target=\"_blank\">" + r.TenantID + "</a>";
            tr.Cells.Add(c);

            //Nama Tenant
            c = new TableCell();
            c.Text = Excel ? r.Nama : "<a href=\"/CS/CustomerFile.aspx?id=" + r.CustomerID + "\" target=\"_blank\">" + r.Nama + "</a>";
            tr.Cells.Add(c);

            //Lokasi
            c = new TableCell();
            c.Text = r.Lokasi ;
            tr.Cells.Add(c);

            //Unit
            c = new TableCell();
            c.Text = Excel ? "'" + r.Unit : r.Unit;
            tr.Cells.Add(c);

            //Periode
            c = new TableCell();
            c.Text = Cf.Tgl(r.TglMulai) + " s/d " + Cf.Tgl(r.TglSelesai);
            tr.Cells.Add(c);

            //Luas
            c = new TableCell();
            c.Text = Cf.Num(r.Luas)+" m2";
            tr.Cells.Add(c);
        }
    }

    protected void lokasi_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (valid)
        fill(false);
    }

    protected void dept_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (valid)
        fill(false);
    }
    protected void strata_SelectedIndexChanged(object sender, EventArgs e)
    {
        Response.ClearContent();
        if (valid)
            fill(false);
    }
}
