﻿using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Drawing;
using System.Linq.Dynamic;

public partial class GL_Default : System.Web.UI.Page
{
    DbDataContext db = new DbDataContext();
    protected string AppModul { get { return "General Ledger"; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        App.SetPage(this);
        App.SetModul(this, AppModul);
        Auth.SafeGuard(this, "Gl");

        if (!IsPostBack)
        {
            App.SetNav(AppModul, alaporan, areminder, aprint);

            fiskal.InnerHtml = Param.Fiskal;
            LibGL.ListSumber(sumber);
            //LibGL.GlPers(pers, LibGL.ListAksesPers(Auth.UserID));
            LibGL.ListPers(pers);
            fill();

            //Response.Write("11 => " + Money.StrVie(11, "dong") + "<br/>");
            //Response.Write("25 => " + Money.StrVie(25, "dong") + "<br/>");
            //Response.Write("101 => " + Money.StrVie(101, "dong") + "<br/>");
            //Response.Write("1001 => " + Money.StrVie(1001, "dong") + "<br/>");
            //Response.Write("2100 => " + Money.StrVie(2100, "dong") + "<br/>");
            //Response.Write("11000 => " + Money.StrVie(11000, "dong") + "<br/>");
            //Response.Write("11011 => " + Money.StrVie(11011, "dong") + "<br/>");
            //Response.Write("101101 => " + Money.StrVie(101101, "dong") + "<br/>");
        }
    }
    protected void sumber_Change(object sender, EventArgs e)
    {
        fill();
    }
    protected void pers_Change(object sender, EventArgs e)
    {
        fill();
    }
    protected void fill()
    {
        for (int i = 1; i <= 12; i++)
        {
            if (!Response.IsClientConnected) break;

            int thn = Param.FiskalTahun;

            TableRow tr = new TableRow();
            if (i == Param.FiskalBulan) tr.CssClass = "highlight";
            tb.Rows.Add(tr);
            TableCell c;

            c = new TableCell();
            c.Text = thn.ToString();
            tr.Cells.Add(c);

            c = new TableCell();
            c.Text = i.ToString();
            tr.Cells.Add(c);

            fill(tr, thn, i);
        }
    }
    protected void fill(TableRow r, int thn, int bln)
    {
        fill(r, thn, bln, 0);
        fill(r, thn, bln, 1);
        fill(r, thn, bln, 2);
        fill(r, thn, bln, 3);
    }
    protected void fill(TableRow r, int thn, int bln, byte Status)
    {
        string dari = Cf.Tgl(Cf.AwalBulan(bln, thn));
        string sampai = Cf.Tgl(Cf.AkhirBulan(bln, thn));

        int t = 0;
        string q = "";
        if (pers.SelectedIndex == 0)
        {
            if (sumber.SelectedIndex == 0)
                t = db.GlJurnals.Count(p => p.Tgl.Year == thn && p.Tgl.Month == bln && p.Status == Status && LibGL.ListAksesDept(Auth.UserID).Contains(p.Dept));
            else
            {
                t = db.GlJurnals.Count(p => p.Tgl.Year == thn && p.Tgl.Month == bln && p.Status == Status && LibGL.ListAksesDept(Auth.UserID).Contains(p.Dept) && p.Sumber == sumber.SelectedValue);
                q = "&sumber=" + sumber.SelectedValue;
            }
        }
        else
        {
            if (sumber.SelectedIndex == 0)
            {
                t = Db.SingleInteger("SELECT COUNT(JurnalID) FROM GlJurnal WHERE YEAR(Tgl)=" + thn + " AND MONTH(Tgl)=" + bln + " AND Dept IN(SELECT Dept FROM GlDept WHERE Pers='" + pers.SelectedValue + "') AND Status=" + Status + ""
                    + " AND Dept in (Select Dept FROM SecAksesDept WHERE UserID = '" + Auth.UserID + "' AND Granted = '1')");
                q = "&pers=" + pers.SelectedValue + "";
            }
            else
            {

                t = Db.SingleInteger("SELECT COUNT(JurnalID) FROM GlJurnal WHERE YEAR(Tgl)=" + thn + " AND MONTH(Tgl)=" + bln + " AND Dept IN(SELECT Dept FROM GlDept WHERE Pers='" + pers.SelectedValue + "') AND Sumber = '" + sumber.SelectedValue + "' AND Status=" + Status + ""
                    + " AND Dept in (Select Dept FROM SecAksesDept WHERE UserID = '" + Auth.UserID + "' AND Granted = '1')");
                q = "&sumber=" + sumber.SelectedValue + "&pers=" + pers.SelectedValue + "";
            }
        }

        TableCell c = new TableCell();
        c.Text = t == 0 ? "" :
            "<a href=\"Jurnal.aspx?status=" + Status + "&dari=" + dari + "&sampai=" + sampai + q + "\" class=\"count\">" + t + "</a>";
        c.CssClass = "center";
        r.Cells.Add(c);
    }
}
